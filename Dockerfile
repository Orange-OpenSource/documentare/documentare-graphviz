FROM debian:bullseye-slim

LABEL maintainer="denis.boisset@orange.com & christophe.maldivi@orange.com"

# Do not clean package cache
# which is required for debian package build
# Create "/usr/share/man/man1" manually due to debian-slim issue (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199)
# hadolint ignore=DL3008
RUN mkdir -p /usr/share/man/man1 && rm /etc/apt/apt.conf.d/docker-clean && \
    apt-get update && \
    apt-get install --no-install-recommends -y curl patch make devscripts fakeroot debhelper ruby ruby-dev freeglut3-dev libann-dev zlib1g-dev libltdl-dev libexpat1-dev libfontconfig1-dev libgts-dev libfreetype6-dev libgd-tools libgd-dev libgdk-pixbuf2.0-dev libxrender-dev libgtk2.0-dev librsvg2-dev libpopplerkit-dev libgs-dev tk-dev tcl-dev libjpeg-dev libxaw7-dev bison flex swig libperl-dev libgd-dev lua5.2 liblua5.2-dev python-all-dev d-shlibs chrpath libgts-dev libann-dev debian-keyring dh-python guile-3.0-dev ghostscript libargon2-dev libsodium-dev libxml2-dev php-dev python3-dev libwebp-dev build-essential git


