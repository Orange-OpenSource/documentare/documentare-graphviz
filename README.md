# Graphviz
The graphviz package is provided by mainstream distributions, but the default package is missing features we need for documentare.
That is why we rebuild here the package in order to add these features.

We do that with the following process:
 - we build a docker image based on debian, and we had all development packages required during the build for the features we want to have
 - then we just have to rebuild the graphviz package in this docker container


# Missing feature

See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=853468

# Build issue in the original debian package

The build of the original source package is not working, that is why we apply a patch to get it working (see file `fix-gvloadimage_gsi`)

# Docker issue to build a debian package

The debian package build process needs to launch the following command: `apt-cache --no-generate show my-pkg`.

`--no-generate` indicates that we use the package cache directly. The problem is that by default the docker debian image removes this cache automatically by changing the default `apt` configuration.

That is why we remove the following file at the beginning of the Dockerfile : `rm /etc/apt/apt.conf.d/docker-clean`
