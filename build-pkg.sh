#!/bin/sh

export DEBFULLNAME="Christophe Maldivi"
export DEBEMAIL="christophe.maldivi@orange.com"


apt-cache --no-generate show zlib1g-dev && \
\
dget http://cdn-fastly.deb.debian.org/debian/pool/main/g/graphviz/graphviz_2.42.2-6.dsc && \
(cd graphviz-2.42.2 && dch -R "rebuild with gts dependency for triangulation" && debuild -us -uc -b) && \
echo do build
